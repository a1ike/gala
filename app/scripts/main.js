$(document).ready(function() {

  $('.phone').inputmask('+7(999)-999-99-99');

  var homeSwiper = new Swiper('.g-home-big', {
    loop: true,
    autoplay: {
      delay: 3000,
    },
    pagination: {
      el: '.g-home-big .swiper-pagination',
      clickable: true,
    }
  });

  homeSwiper.on('slideChange', function() {
    $('.g-home-big__counter-current').html(++this.realIndex);
  });

  $('.g-home-big__counter-total').html($('.g-home-big__card').length-2);

  new Swiper('.g-home-projects__cards', {
    slidesPerView: 4,
    spaceBetween: 30,
    /* centeredSlides: true, */
    navigation: {
      nextEl: '.g-home-projects__next',
      prevEl: '.g-home-projects__prev',
    },
    pagination: {
      el: '.g-home-projects__cards .swiper-pagination',
      clickable: true,
    },
    breakpoints: {
      1200: {
        slidesPerView: 1
      }
    }
  });

  new Swiper('.g-home-partners__cards', {
    slidesPerView: 4,
    slidesPerColumn: 2,
    navigation: {
      nextEl: '.g-home-partners__next',
      prevEl: '.g-home-partners__prev',
    },
    breakpoints: {
      1200: {
        slidesPerView: 1
      }
    }
  });

  new Swiper('.g-home-articles__cards', {
    slidesPerView: 2,
    spaceBetween: 120,
    loop: true,
    navigation: {
      nextEl: '.g-home-articles__next',
      prevEl: '.g-home-articles__prev',
    },
    pagination: {
      el: '.g-home-articles__cards .swiper-pagination',
      clickable: true,
    },
    breakpoints: {
      1200: {
        slidesPerView: 1
      }
    }
  });

  new Swiper('.g-papers__cards', {
    slidesPerView: 3,
    loop: true,
    navigation: {
      nextEl: '.g-papers__next',
      prevEl: '.g-papers__prev',
    },
    pagination: {
      el: '.g-papers__cards .swiper-pagination',
      clickable: true,
    },
    breakpoints: {
      1200: {
        slidesPerView: 1
      }
    }
  });

  new Swiper('.g-object__big-cards', {
    loop: true,
    autoplay: {
      delay: 3000,
    },
    navigation: {
      nextEl: '.g-object__big-next',
      prevEl: '.g-object__big-prev',
    },
    pagination: {
      el: '.g-object__big .swiper-pagination',
      clickable: true,
    }
  });

  $('.open-modal').on('click', function(e) {
    e.preventDefault();
    $('.g-modal').toggle();
  });

  $('.g-modal__close').on('click', function(e) {
    e.preventDefault();
    $('.g-modal').toggle();
  });

  $('.g-header__mob').on('click', function(e) {
    e.preventDefault();
    $('.g-header__nav').slideToggle('fast');
    $('.g-top').slideToggle('fast');
  });

  $('.g-goods__card-plus').on('click', function(e) {
    e.preventDefault();

    var currentValue = $('.g-goods__card-counter input').val();

    if (currentValue < 1000) {
      $(this).next().val(++currentValue);
    }
  });

  $('.g-goods__card-minus').on('click', function(e) {
    e.preventDefault();

    var currentValue = $('.g-goods__card-counter input').val();

    if (currentValue > 1) {
      $(this).prev().val(--currentValue);
    }
  });

  $('.g-item__main-plus').on('click', function(e) {
    e.preventDefault();

    var currentValue = $('.g-item__main-counter input').val();

    if (currentValue < 1000) {
      $(this).next().val(++currentValue);
    }
  });

  $('.g-item__main-minus').on('click', function(e) {
    e.preventDefault();

    var currentValue = $('.g-item__main-counter input').val();

    if (currentValue > 1) {
      $(this).prev().val(--currentValue);
    }
  });

  $('.g-basket-card__plus').on('click', function(e) {
    e.preventDefault();

    var currentValue = $('.g-basket-card__counter input').val();

    if (currentValue < 1000) {
      $(this).next().val(++currentValue);
    }
  });

  $('.g-basket-card__minus').on('click', function(e) {
    e.preventDefault();

    var currentValue = $('.g-basket-card__counter input').val();

    if (currentValue > 1) {
      $(this).prev().val(--currentValue);
    }
  });

  $('.g-tabs__header li').click(function() {
    var tab_id = $(this).attr('data-tab');

    $('.g-tabs__header li').removeClass('current');
    $('.g-tabs__content').removeClass('current');

    $(this).addClass('current');
    $('#' + tab_id).addClass('current');
  })
});